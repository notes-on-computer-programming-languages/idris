# idris

[idris-lang/Idris2](https://github.com/idris-lang/Idris2) @ GitHub

* [Repology: idris](https://repology.org/projects/?search=idris)

* [docs.idris-lang.org](http://docs.idris-lang.org)
* [Index of /docs](https://www.idris-lang.org/docs/)

* [idris-lang/Idris-dev](https://github.com/idris-lang/Idris-dev) @ GitHub
  * [/libs](https://github.com/idris-lang/Idris-dev/tree/master/libs)

* [*Idris_(programming_language)*](https://en.m.wikipedia.org/wiki/Idris_(programming_language))

* [*Idris for Haskellers*
  ](https://github.com/idris-lang/Idris-dev/wiki/Idris-for-Haskellers)
* [*Interactive Editing*
  ](http://docs.idris-lang.org/en/latest/tutorial/interactive.html)


# Download
* https://github.com/asdf-community/asdf-idris2
* https://github.com/edwinb/Idris2
* https://github.com/idris-lang/Idris2
* https://pkgs.alpinelinux.org/packages?name=idris2
* idris2 in Nix: `nix-env -i idris2`


# Official documentation
* [Index of /docs/idris2/current](https://www.idris-lang.org/docs/idris2/current/)
* [*Type-Driven Development with Idris*
  ](https://www.manning.com/books/type-driven-development-with-idris)
  2017 Edwin Brady
  * [*Type Driven Development with Idris: Updates Required*
    ](https://idris2.readthedocs.io/en/latest/typedd/typedd.html)
    * https://github.com/idris-lang/Idris2/tree/main/tests/typedd-book
    * https://github.com/edwinb/Idris2/tree/main/tests/typedd-book
  * [Type-Driven Development with Idris](https://worldcat.org/search?q=Type-Driven+Development+with+Idris)
* [*State Machines All The Way Down: An Architecture for Dependently Typed Applications*
  ](https://www.idris-lang.org/drafts/sms.pdf)
  2016-01 Edwin Brady

## Special topics
* Docs » A Crash Course in Idris 2 »
  [*Interactive Editing*
  ](https://idris2.readthedocs.io/en/latest/tutorial/interactive.html)
* Docs » A Crash Course in Idris 2 »
  [*Types and Functions*
  ](https://idris2.readthedocs.io/en/latest/tutorial/typesfuns.html)
  » [*Holes*
  ](https://idris2.readthedocs.io/en/latest/tutorial/typesfuns.html#holes)
* [*Idris for Haskellers*
  ](https://github.com/idris-lang/Idris-dev/wiki/Idris-for-Haskellers)
  @ Wiki

# Books
* [*Introduction to dependent types with Idris : encoding program proofs in types*
  ](https://worldcat.org/en/search?q=Introduction+to+Dependent+Types+with+Idris%3A+Encoding+Program+Proofs+in+Types)
  2023 Boro Sitnikovski
* [*Gentle introduction to dependent types with idris*
  ](https://worldcat.org/en/search?q=Gentle+introduction+to+dependent+types+with+idris)
  2019 Boro Sitnikovski

# Libraries
* [idris-lang/Idris2](https://github.com/idris-lang/Idris2) @ GitHub
  * [/libs](https://github.com/idris-lang/Idris2/tree/main/libs)
* [The Top 16 Idris Idris2 Open Source Projects on Github](https://awesomeopensource.com/projects/idris/idris2)
  @ Awesome Open Source
* [The Top 563 Idris Open Source Projects on Github
  ](https://awesomeopensource.com/projects/idris)
  @ Awesome Open Source

# Unofficial documentation
* [*Dependent types and Idris*
  ](https://functional.christmas/2020/9)
  2020-12 Isak Sunde Singh
* [*Idris state machines in JavaScript apps*
  ](https://medium.com/the-web-tub/idris-state-machines-in-javascript-apps-b969e2cb6ed2)
  2018-04 Eric Corson
* [*A small Idris web application: to-do list*
  ](https://www.reddit.com/r/Idris/comments/8yb5xt/a_small_idris_web_application_todo_list/)
  (2018) @ Reddit
* [*10 things Idris improved over Haskell*
  ](https://deque.blog/2017/06/14/10-things-idris-improved-over-haskell/)
  2017-06 @ Deque

# Compiler design
* [*Cross-platform Compilers for Functional Languages: Research Paper*
  ](https://eb.host.cs.st-andrews.ac.uk/drafts/compile-idris.pdf)
  ? Edwin Brady

# Scheme implementation: nanopass framework
* [*Why is Idris 2 so much faster than Idris 1?*
  ](https://news.ycombinator.com/item?id=23304081)
  (2020-05) @ Hacker News
* [*Racket-on-Chez Status: February 2020*
  ](https://blog.racket-lang.org/2020/02/racket-on-chez-status.html)
  2020-02 Matthew Flatt
* [*Chez Scheme is written using the nanopass framework*
  ](https://news.ycombinator.com/item?id=15156027)
  2017-09 capnrefsmmat @ Hacker News



---
---
---
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/notes-on-computer-programming-languages/idris.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/notes-on-computer-programming-languages/idris/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
